from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in hcms_core/__init__.py
from hcms_core import __version__ as version

setup(
	name="hcms_core",
	version=version,
	description="Core functions shareable between modules",
	author="developer@masterisehomes.com",
	author_email="developer@masterisehomes.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
