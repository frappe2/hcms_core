from frappe import _

def get_data():
	return [
		{
			"module_name": "HCMS Core",
			"type": "module",
			"label": _("HCMS Core")
		}
	]
