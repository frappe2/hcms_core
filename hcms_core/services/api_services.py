from hcms_core.utils.constants import CoreConstants
from hcms_core.services.api_response import APIResponse


class APIService:
    def __init__(self):
        self.timeout = CoreConstants.timeout_default


    def handle_request(self, func) -> APIResponse:
        try:
            return func()
        except BaseException as e:
            raise(e)

    