import frappe

class TranslationService:
    
    @staticmethod
    def translate(content : str, lang : str = 'vi', context : str = None) -> str:
        if (frappe.db.exists('Translation', {'language': lang, 'source_text': content, 'context': context})):
            return frappe.db.get_value(
                'Translation', 
                {'language': lang, 'source_text': content, 'context': context},
                'translated_text',
            )
        else:
            return None




