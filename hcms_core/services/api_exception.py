class APIException(BaseException):
    def __init__(self, message : str, result : int = 0) -> None:
        self.message = message
        self.result = result

    def __str__(self) -> str:
        return self.message


class BadRequestException(APIException):
    def __init__(self, message: str, result: int = 0) -> None:
        super().__init__(message, result)


class UnauthorisedException(APIException):
    def __init__(self, message: str, result: int = 0) -> None:
        super().__init__(message, result)


class InvalidInputException(APIException):
    def __init__(self, message: str, result: int = 0) -> None:
        super().__init__(message, result)


class NotFoundException(APIException):
    def __init__(self, message: str, result: int = 0) -> None:
        super().__init__(message, result)


class OTPMessageException(APIException):
    def __init__(self, message: str, result: int = 0) -> None:
        super().__init__(message, result)