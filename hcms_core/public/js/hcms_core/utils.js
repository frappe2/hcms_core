
// if (!window.hcms_core) window.hcms_core = {};

frappe.provide("hcms_core.utils");

hcms_core.utils = {
    execute: async function(frm, callback){
        console.log('hcms_core.utils called')
        console.log(frm.is_dirty())
        if (frm.is_dirty()){

			frappe.ui.form.save(frm, "Save", (r) => {
				if (r && r.exception === undefined){
					callback()
				}
			})

        }else{
            console.log('hcms_core.utils execute callback saved')
            callback()
        }
    },

	translate: async function(content, lang = 'vi', context = null){
		let filters = {'language': lang, 'source_text': content}
		if (context !== null){
			filters.set('context', context)
		}
		let response = await Promise.resolve(frappe.db.get_value('Translation', filters, 'translated_text'))
		console.log(`translate ${content}`)
		console.log(response)
		return response.message.translated_text;
	},


    

}