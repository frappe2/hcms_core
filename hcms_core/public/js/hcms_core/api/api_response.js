class APIResponse {
    constructor(result, message, data, total){
        this.result = result
        this.message = message
        this.data = data
        this.total = total
    }

    fromJson(map){
        return {
            result: this.result,
            message: this.message,
            data: this.data,
            total: this.total
        }
    }
}