import re
from hcms_core.utils.constants import CoreConstants
import frappe

class CoreUtils:

    @staticmethod
    def send_email(emails, message, subject):
        frappe.sendmail(
        recipients=emails,
        subject=subject,
        message=message,
        now=True,
    )

    @staticmethod
    def format_date_dmy(dateStr) -> str:
        return dateStr.strftime("%d/%m/%Y")


    @staticmethod
    def convert_to_non_accent_vietnamese(txt="") -> str:
        txt = txt.lower()
        txt = txt.strip()
        txt = re.sub("à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ", "a", txt)
        txt = re.sub("è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ", "e", txt)
        txt = re.sub("ì|í|ị|ỉ|ĩ", "i", txt)
        txt = re.sub("ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ", "o", txt)
        txt = re.sub("ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ", "u", txt)
        txt = re.sub("ỳ|ý|ỵ|ỷ|ỹ", "y", txt)
        txt = re.sub("đ", "d", txt)
        txt = re.sub(" ", "", txt)
        txt = re.sub("\u0300|\u0301|\u0303|\u0309|\u0323", "", txt)
        txt = re.sub("\u02C6|\u0306|\u031B", "", txt)

        return txt

    @staticmethod
    def reformat_phone_number(phone : str) -> str:
        if (not phone):
            raise ValueError(CoreConstants.ErrorMessages.InvalidPhoneNumber)

        if (len(phone) <= 3):
            raise ValueError(CoreConstants.ErrorMessages.InvalidPhoneNumber)

        # remove country code - anything between +..-
        return re.sub("\+.*-", "", phone)
        


        


