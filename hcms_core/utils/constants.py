
class CoreConstants:
    timeout_default : int = 60
    phone_code_vn = "+84"
    
    class DoctypeName:
        Employee : str = "Employee"
        User : str = "User"
        Workflow : str = "Workflow"
        UserPermission : str = "User Permission"
        Contract : str = "Contract"
        JobPosition : str = "Job Position"
        Department : str = "Department"
        Company : str = "Company"
        PositionBand : str = "Position Band"
        File : str = "File"
        HasRole : str = "Has Role"
        EmailTemplate : str = "Email Template"
        Ward : str = 'Ward'
        District : str = 'District'
        Province : str = 'Province'
        BusinessRepresentative : str = 'Business Representative'
        Template : str = 'Template'

    class APIDomain:
        DigiSign : str = "https://digisign-crm-new.masterisehomes.com:10001"
        LDAPPortal : str = "https://masterise-portal-api-ldap.masterisehomes.com:6789"
        SMS : str = "https://api-gateway-1.masterisehomes.com:18001"

    class APIPath:
        SMS : str = "/freeSendSmsReq"
        CustomerSign : str = "/freeDigiSign_CRM_New_Customer_Req"
        CopmpanySign : str = "/freeDigiSign_CRM_New_Company_Req"

    class APIStatusCode:
        Success : int = 200
        InternalServerError : int = 500

    class ErrorMessages:
        InvalidPhoneNumber : str = "Invalid Phone number"

    class LanguageCode:
        VN : str = 'vn'
        EN : str = 'en'



